const MuellPaste = require("./paste");

/**
 * Routes
 */
module.exports = function(app, db) {

    /**
     * GET paste
     * @param {string} id - id of paste as hexdigest or url-encoded base64
     * Returns JSON paste data or success
     */
    app.get("/api/get", (req, res) => {
        var returnData = {
            "success" : false
        };

        if(!req.query.id) {
            returnData.msg = "missing param";
            res.send(returnData);
            return;
        }

        paste = new MuellPaste;
        paste.data.paste_id = req.query.id;

        db.getPaste(paste, function(paste) {
            if(paste === false) {
                res.send(returnData);
            }
            else {
                paste.data.success = true;
                res.send(paste.data);
            }
            return;
        });


    });


    /**
     * PUT (create) paste
     * @param {string} content - base64-encoded, encrypted content
     * @param {string} title - Title of paste
     * @param {string} iv - AES iv, hexdigest
     * Returns success and paste_id (hexdigest)
     */
    app.post("/api/create", (req, res) => {

        paste = new MuellPaste;
        paste.data.content = req.body.content;
        paste.data.title = req.body.title;
        paste.data.iv = req.body.iv;

        db.createPaste(paste, function(paste) {
            if (paste === false) {
                var returnData = { "success" : false };
            }
            else {
                var returnData = {
                    "success" : true,
                    "paste_id" : paste.data.paste_id
                };
            }
            res.send(returnData);
            return;
        });
        /*.then( function() {
            res.send(returnData);
            return;
        });*/

    });

};
