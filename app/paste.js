var forge = require('node-forge');

/**
 * Paste
 */
class Paste {
    /**
     * initializes empty, apart from timestamp and user_id
     * @constructor
     */
    constructor() {
        this.data = {
            "paste_id"  :   "",
            "timestamp" :   Date.now(),
            "user_id"   :   0,          // anon has id 0
            "title"     :   "",
            "content"   :   "",
            "iv"        :   ""
        };

    }

    /**
     * Calculate / generate random paste id
     * id is a md5 hexdigest
     */
    calcPasteId() {
        var md = forge.md.md5.create();
        md.update(forge.random.getBytesSync(32));
        this.data.paste_id = md.digest().toHex();
    }

    /**
     * Check for a valid id
     * can be either md5 hexdigest or url-encoded base64 hexdigest
     */
    checkValidityId() {
        var idEx_md5 = /^[0-9A-Fa-f]{32}$/g;
        if (!idEx_md5.test(this.data.paste_id)) {

            try {
                this.data.paste_id = this.data.paste_id.replace(/ /g, "+");
                var id_new = forge.util.bytesToHex(forge.util.decode64(this.data.paste_id));
            }
            catch(err) {
                return false;
            }

            if (!idEx_md5.test(id_new)) {
                console.log(id_new);
                return false;
            }
            else {
                this.data.paste_id = id_new;
            }
        }
        return true;
    }

    /**
     * Perform sanity check on all paste data
     */
    checkValidity() {

        if (! this.checkValidityId()) {
            console.log("invalid id");
            return false;
        }

        var ivEx = /^[0-9A-Fa-f]{32}$/g;
        if (!ivEx.test(this.data.iv)) {
            console.log("invalid iv");
            return false;
        }

        var contentEx = /^[0-9A-Fa-f]{1,}$/g;
        if (!contentEx.test(this.data.content)) {
            console.log("invalid content");
            return false;
        }

        return true;
    }

}

module.exports = Paste;
