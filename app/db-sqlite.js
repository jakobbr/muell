const sqlite3 = require("sqlite3").verbose();
const config = require("config");


/**
 * Database Interface
 */
class DBInterface {

    /**
     * Load config and create sqlite3.Databse
     */
    constructor() {
        this.dbConfig = config.get("database");
        this.db = new sqlite3.Database(config.get("database.sqlite3.filename"));

        this.initDb();

    }

    /**
     * Inizialize Database. Check if table exists, else create.
     */
    initDb() {

        var dbi = this; // this doesn't look smart...

        this.checkTable(function(err) {
            if (err) {
                dbi.createTable(dbi);
            }
        });

    }

    /**
     * Check if table 'pastes' exists.
     * TODO: Also check if table is good.
     */
    checkTable(callback) {
        var tableStatus = false;
        var q = "SELECT `paste_id` FROM `pastes` LIMIT 1;";
        this.db.run(q, {}, function(err) {
            callback(err);
        });
    }

    /**
     * Create table 'pastes'
     * TODO: Allow configurable prefix/table name?
     * @param {DBInterface} dbi
     */
    createTable(dbi) {
        var q = "DROP TABLE IF EXISTS `pastes`; \
                 CREATE TABLE `pastes` \
                 ( `paste_id` VARCHAR(64) UNIQUE, \
                   `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , \
                   `user_id` INT NOT NULL , \
                   `title` TEXT NOT NULL , \
                   `content` TEXT NOT NULL , \
                   `iv` VARCHAR(32) NOT NULL );";
        dbi.db.exec(q, function(err) {
            if (err) {
                throw Error(err);
            }
        });
    }

    /**
     * Insert paste into database.
     * Fails if invalid data.
     * @param {MuellPaste} paste
     */
    createPaste(paste, callback) {

        paste.calcPasteId();
        if(!paste.checkValidity()) {
            callback(false);
        }

        else {

            var q = "INSERT INTO `pastes` \
                     (`paste_id`, `timestamp`, \
                      `user_id`, `title`, \
                      `content`, `iv`) \
                     VALUES (?, CURRENT_TIMESTAMP, ?, ?, ?, ?);";

            var data = [
                paste.data.paste_id,
                paste.data.user_id,
                paste.data.title,
                paste.data.content,
                paste.data.iv
            ];

            this.db.run(q, data, function(err) {
                if (err) {
                    throw Error(err);
                }
                callback(paste);
            });

        }

    };

    /**
     * Retrieve paste from database.
     * Fails if invalid id or not found.
     * @param {MuellPaste} paste - paste (only id needs to be set)
     */
    getPaste(paste, callback) {

        if (!paste.checkValidityId()) {
            console.log("invlid id");
            callback(false);
        }
        else {

            var q = "SELECT * FROM `pastes` WHERE `paste_id` = ?;";
            var row;
            this.db.get(q, paste.data.paste_id, function(err, row) {

                if(err) {
                    return false;
                }

                if(row) {
                    paste.data = row;
                    callback(paste);
                }
                else {
                    console.log("not found");
                    callback(false);
                }

            });
        }

    };


}

module.exports = DBInterface;
