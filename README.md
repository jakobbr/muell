# Müll
### A simple pastebin.

-----------

Müll is a simple pastebin-type webapp built on NodeJS.

## Key Features

* in-browser encryption, zero knowledge
* syntax highlighting
* minimalist design
* easily deployable to Plesk

## Running Müll locally

1. Requires NodeJS (tested on versions 9 and 10).
Debian/Ubuntu:
```sh
sudo snap install node --classic --channel=10
```

2. Clone Muell repo
```sh
git clone <repo>
cd muell
```

3. Install Node packages
```sh
npm install
```

4. Configure Port
```sh
export PORT=3001
```
(also, you can use the `fallback_port` option in the config file,
this will be used if the envvar is undefined)

5. Run Müll
```sh
npm start
```

6. Choose a logo/mascot and place into
`static/img/logo.png`.
Alternatively, rename the default one
`logo-default.png` to `logo.png`.

## Deploying Müll to Plesk

Tested on Plesk 17.

**Set up Git for the desired (sub-)domain.**

For example, we'll want to set up `paste.example.com`.

It's up to you whether you want Plesk to pull the repo or you want to push the repo to Plesk.
I use the latter option.
Make sure it's deploying to a useful location along the lines of
`/paste.example.com/muell`,
add the remote, (force) push.

**Set up NodeJS for the desired (sub-)domain.**

ABSOLUTELY make sure your document root is the `static` directory!
If you do not do this, nginx/Apache will serve other files, including CONFIGURATION and the DATABASE!

The startup file is `app.js`.

When configured, enable NodeJS and run 'NPM Install',
now you'll have Müll running on Plesk.

## Configuring Müll

Müll uses `node-config`. See their [their docs](https://github.com/lorenwest/node-config/wiki/Configuration-Files#file-load-order) for more information.

Just copy `default.json` to sth. like `local.json` and configure there.

## Developing Müll

### Running via supervisor
```sh
sudo -H npm install -g supervisor
npm run dev
```

### Testing
Test backend with:
```sh
npm test
```

Test frontend with:
```sh
npm run test-front
```

For frontend testing, `mocha-headless-chrome` must be installed and
chromium's permissions might have to be fixed:
```sh
npm install -g mocha-headless-chrome

cd /usr/lib/node_modules/mocha-headless-chrome/node_modules/puppeteer/.local-chromium/
find . -type d | xargs -L1 -Ixx sudo chmod 755 xx
find . -type f -perm /u+x | xargs -L1 -Ixx sudo chmod 755 xx
find . -type f -not -perm /u+x | xargs -L1 -Ixx sudo chmod 644 xx
```


### Generating docs
```sh
sudo -H npm install -g jsdoc
mkdir doc
npm run jsdoc
```
