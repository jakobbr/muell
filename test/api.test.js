var expect = require("chai").expect;
var request = require("request");

var Muell = require("../app");
var app = Muell.app;
var server = Muell.server;

var baseUrl = "http://localhost:" + server.address().port;

var util = require("util");

var testPasteData = {
    title: "Test",
    content: "a654d43cdcfd2258549c8a1811246bbf",
    iv: "0cc05edb5e5de66021b2d7225b231366"
};

describe("Backend/API", function() {

    it("index 200 okay", function(done) {
        request(baseUrl, function(err, res, body) {
            expect(res.statusCode).to.equal(200);
            done();
        });
    });

    var testPasteId = "";
    it("insert paste", function(done) {

        var postReqData = {
            url: baseUrl + "/api/create",
            form: testPasteData
        };

        request.post(postReqData, function(err, res, body) {
            var pBody = JSON.parse(body);
            testPasteId = pBody.paste_id;
            expect(pBody.success).to.equal(true);
            done();
        });

    });


    it("get a paste", function() {
        var getReqData = {
            url: baseUrl + "/api/get",
            qs: {
                id: testPasteId
            }
        };

        request.get(getReqData, function(err, res, body) {
            var pBody = JSON.parse(body);
            var params = Object.keys(testPasteData);
            var returnedDataConflicts = false;
            params.forEach(function(param) {
                returnedDataConflicts = returnedDataConflicts || (
                        pBody[param] !== testPasteData[param]
                        );
            });
            expect(returnedDataConflicts).to.equal(false);
        });
    });



});
