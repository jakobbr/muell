var expect = require("chai").expect;
var MuellPaste = require("../app/paste.js");

describe("MuellPaste", function() {
    describe("new MuellPaste", function() {
        it("should initialize a MuellPaste", function() {
            var p = new MuellPaste;
            expect(p);
        });
    });
    describe("calculate paste id", function() {
        it("should set a valid id", function() {
            let p = new MuellPaste;
            p.calcPasteId();
            expect(p.checkValidityId()).to.equal(true);
        });
    });
    describe("checkValidityId", function() {
        var testIds = {
            "b10a8db164e0754105b7a99be72e3fe5" : [true, "valid md5"],
            "zI/EcFmZLLhkGnsdPPLYjQ==" : [true, "valid b64"],

            "10a8db164e0754105b7a99be72e3fe5" : [false, "too short md5"],
            "1000a8db164e0754105b7a99be72e3fe5" : [false, "too long md5"],
            "EcFmZLLhkGnsdPPLYjQ==" : [false, "too short b64"],
            "zzI/EcFmZLLhkGnsdPPLYjQ==" : [false, "too long b64"],

            "x10a88b164e0754105b7a99be72e3fe5" : [false, "md5 with invalid charset"],
            ";10a88b164e0754105b7a99be72e3fe5" : [false, "md5 with invalid charset"],
            "-10a88b164e0754105b7a99be72e3fe5" : [false, "md5 with invalid charset"]

        };

        var ids = Object.keys(testIds);

        ids.forEach(function(id) {
            let expected = testIds[id][0];
            let info = testIds[id][1];
            let action;
            if(expected === true) {
                action = "accept";
            }
            else {
                action = "reject";
            }
            it("should " + action + " " + info, function() {
                let p = new MuellPaste;
                p.data.paste_id = id;
                expect(p.checkValidityId()).to.equal(expected);
            });
        });

    });

    describe("checkValidity --> iv", function() {
        var testIvs = {
            "b10a8db164e0754105b7a99be72e3fe5" : [true, "valid iv"],
            "10a8db164e0754105b7a99be72e3fe5" : [false, "too short iv"],
            "bb10a8db164e0754105b7a99be72e3fe5" : [false, "too long iv"],
            "x10a8db164e0754105b7a99be72e3fe5" : [false, "iv with invalid charset"]
        }
        var validContent = "b10a8db164e0754105b7a99be72e3fe5";

        var ivs = Object.keys(testIvs);

        ivs.forEach(function(iv) {
            let expected = testIvs[iv][0];
            let info = testIvs[iv][1];
            it("should deal with " + info, function() {
                let p = new MuellPaste;
                p.calcPasteId();
                p.data.iv = iv;
                p.data.content = validContent;
                expect(p.checkValidity()).to.equal(expected);
            });
        });
    });
    describe("checkValidity --> content", function() {
        let validIv = "b10a8db164e0754105b7a99be72e3fe5";
        let forge = require('node-forge');
        let md = forge.md.md5.create();
        md.update(forge.random.getBytesSync(1024));
        let validContent = md.digest().toHex();
        let invalidContent = validContent + "x";

        it("should accept valid content", function() {
            let p = new MuellPaste;
            p.data.iv = validIv;
            p.calcPasteId();
            p.data.content = validContent;
            expect(p.checkValidity()).to.equal(true);
        });
        it("should reject invalid content", function() {
            let p = new MuellPaste;
            p.data.iv = validIv;
            p.calcPasteId();
            p.data.content = invalidContent;
            expect(p.checkValidity()).to.equal(false);
        });

    });
});
