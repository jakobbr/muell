/*jshint browser: true*/
/*jshint esversion: 6 */
/*global $*/
/*global forge*/
/*global hljs*/
/*global moment*/

/**
 * Müll Frontend
 */

"use strict";


/**
 * Section: DOM-RELATED
 */

/**
 * Resize code view according to fill bootstrap column.
 */
function resizeCodeView() {
    var codeViewWidth = $(".col-codeview").width(),
        lineNoWidth = $(".line-number").width();
    $(".line-code pre").width(codeViewWidth - lineNoWidth - 10);
}

/**
 * Get line count of a string
 * @param {string} text
 * @returns {int} length
 */
function getTextLineCount(text) {
    return text.split(/\r\n|\r|\n/).length;
}

/**
 * Display lines of text
 * @param {string} text
 * @param {int} lineCount - optional
 */
function displayLinesText(text, lineCount) {

    if (lineCount === undefined) {
        lineCount = getTextLineCount(text);
    }

    if (text.slice(-1) === "\n" || text.length === 0) {
        text += "\n"; // Properly display last, empty line
    }

    $(".line-code pre").text(text);

    var highlightThreshold = 2000;
    if (lineCount < highlightThreshold) {
        $("pre").each(function (i, block) {
            hljs.highlightBlock(block);
        });
    }

}

/**
 * Display line numbers
 * @param {int} lineCount
 */
function displayLinesNumbers(lineCount) {

    var lineNumbers = "";

    for (let n = 1; n <= lineCount; n++) {
        lineNumbers += n;
        lineNumbers += "\n";
    }

    $(".line-number pre").text(lineNumbers);

}

/**
 * Display text and lines of a paste
 * @param {string} text
 */
function displayPasteText(text) {
    var lineCount = getTextLineCount(text);
    displayLinesText(text, lineCount);
    displayLinesNumbers(lineCount);

}

/**
 * Display paste metadata:
 * title, timestamp
 * @param {Object} paste
 */
function displayPasteMeta(paste) {
    $("#filename").text(paste.title);
    $(document).attr("title", "Müll: " + $( "#filename" ).text());

    var ts = moment.utc(paste.timestamp);
    var ago = "";

    if ( ! ts.isValid() ) {
        ago = "invalid date";
    }
    else {
        ago = ts.local().format("lll") + " (" + ts.fromNow() + ")";
    }

    $("#date").text(ago);

}

/**
 * Display logo if window wide enough
 */
function displayLogo() {
    if ( $( window ).width() >= 1570) {
        $( "#mascot" ).show();
    }
    else {
        $( "#mascot" ).hide();
    }
}

/**
 * Display an error message
 * @param {string} title - will become bold
 * @param {string} message - (optional)
 */
function displayError(title, message="") {
    $( "#error strong" ).text(title);
    $( "#error span" ).text(message);
    $( "#error" ).show();
}

/**
 * Set up DOM to allow for paste editing
 */
function setupPasteEditor() {

    $(".line-code pre" ).html("<textarea id='text-input'></textarea>");
    $("#filename").html("<input class='input-highlight' id='filename-input'></input>");

    $(".btn-edit").show();

    $(".line-code pre").addClass("input-highlight");

    $(".line-number pre").css("padding-bottom", "+=21.5");

    $("#text-input").on("change input keyup paste", function() {
        var text = $("#text-input").val();
        var lineCount = getTextLineCount(text);
        displayLinesNumbers(lineCount);
        $("#text-input").height( $(".line-number pre").height() + 20 );
    });

    $("#text-input").trigger("change");

    $("#btn-upload").click(uploadPaste);
}

/**
 * Section: API INTERACTION
 */

/**
 * Fetch a paste
 * @param {str} pasteId - hex or b64
 * @param {function} callback
 */
function fetchPaste(pasteId, callback) {

    $.getJSON("/api/get?id=" + pasteId, function(data) {
        if (!data.success) {
            displayError("Can't fetch this paste.");
        }
        callback(data);
    });

}

/**
 * Upload the paste being edited
 * @param {bool} dryRun - don't send request, return encrypted data (testing)
 */
function uploadPaste(dry=false) {

    var plainText = $("#text-input").val();

    var encryptedPaste = encryptPaste(plainText);

    encryptedPaste.title = $("#filename-input").val();

    var key = encryptedPaste.key;
    encryptedPaste.key = null;
    var encodedKey = encodeURIComponent(
            forge.util.encode64(key)
            );

    if (dry === true) {
        return [encryptedPaste, key];
    }

    $.post("/api/create", encryptedPaste, function(res) {

        var paste_id = encodeURIComponent(
                forge.util.encode64(
                        forge.util.hexToBytes(res.paste_id)
                    )
                );

        window.location.href = "?p=" + paste_id + "#" + encodedKey;

    }).fail(function(err) {
        displayError("Creation error", err);
    });

}

/**
 * Section: FORGE/CRYPTO
 */

/**
 * Get key from url, decode base64 to hex
 * @returns {string} key
 */
function getKeyHex() {
    var keyEncoded = window.location.hash.substr(1);
    var key = forge.util.decode64(decodeURIComponent(keyEncoded));
    return key;
}

/**
 * Decrypt paste
 * @param {Object} pasteData
 * @param {function} callback
 */
function decryptPaste(pasteData, callback) {

    var key = forge.util.hexToBytes(getKeyHex());
    var cipher = forge.util.hexToBytes(pasteData.content);
    var iv = forge.util.hexToBytes(pasteData.iv);

    var result = decryptData(cipher, iv, key);

    if (result) {
        callback(result);
    }

}

/**
 * Decrypt data/string
 * @param {Bytes} cipher
 * @param {Bytes} iv
 * @param {Bytes} key
 * @returns {Object} decipher
 */
function decryptData(cipher, iv, key) {
    var decipher;
    try {
        decipher = forge.cipher.createDecipher("AES-CBC", key);
        decipher.start({iv: iv});
        decipher.update(forge.util.createBuffer(cipher));
        var result = decipher.finish();
    }
    catch(err) {
        decipher = false;
        displayError("Decryption failed", err);
        throw err;
    }

    return decipher;

}

/** Encrypt paste.
 * TODO: Encrypt metadata
 * @param {string} text
 * @param {function} callback
 * @returns {Object} encryptedData
 */
function encryptPaste(text, callback) {
    var encryptedData = encryptData(text);
    return encryptedData;
}

/** Encrypt data
 * @param {string} plainText
 * @returns {Object} encryptedData - has content, iv and key as hex
 */
function encryptData(plainText) {

    var key = forge.random.getBytesSync(32);
    var iv = forge.random.getBytesSync(16);
    var cipher = forge.cipher.createCipher('AES-CBC', key);

    cipher.start({iv: iv});
    cipher.update(forge.util.createBuffer(plainText));
    cipher.finish();
    var encrypted = cipher.output;

    var encryptedData = {
        content: encrypted.toHex(),
        iv: forge.util.bytesToHex(iv),
        key: forge.util.bytesToHex(key)
    };

    return encryptedData;
}

/**
 * Section: MAIN FUNCTIONALITY
 */

$( document ).ready(function() {

    // Do visual stuff
    resizeCodeView();
    displayLogo();

    var searchParams = new URLSearchParams(window.location.search);
    if ( searchParams.has("p") ) {
        $ ( ".btn-view" ).show();
        var pasteId = searchParams.get("p");
        fetchPaste(pasteId, function(pasteData) {
            displayPasteMeta(pasteData);
            decryptPaste(pasteData, function(pasteDecipher) {
                displayPasteText(pasteDecipher.output.data);
            });
        });
    }
    else {
        setupPasteEditor();
    }

});


$( window ).on("resize", function() {
    resizeCodeView();
    displayLogo();
});

// Buttons
$( "#btn-create" ).click(function() {
    window.location.href = "?";
});
