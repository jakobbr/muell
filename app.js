const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const config = require("config");
const rateLimit = require("express-rate-limit");

const app = express();

var port = process.env.PORT;  // Plesk requirement
if (port === undefined) {
    console.warn("Port envvar not defined, trying fallback!");
    port = config.get("fallback_port");
    if(port === null) {
        console.warn("No fallback port configured either!");
        port = undefined;
    }
}


var limiterParams = config.get("rate-limiters");

limiterParams.forEach(function(p) {

    const limiter = rateLimit({
            windowMs: p.windowMinutes * 60 * 1000, // 1 minute
            max: p.maxPerWin // allow 30 requests per minute
    });
    if (p.scope === "/") {
        app.use(limiter);
    }
    else {
        app.use(p.scope, limiter);
    }

});


// used to parse POST
var limit = config.get("post-size-limit");
app.use(bodyParser.urlencoded({ limit: limit, extend: true }));

// serve static files
app.use(express.static("static"))


const dbType = config.get("database.type");
var db;

if (dbType == "sqlite3") {
    const DBInterface = require("./app/db-sqlite");
    db = new DBInterface;
}
else {
    throw Error("No database!");
}


require('./app/routes')(app, db);

var server = app.listen(
    port, () => {
        var addr = server.address();
        console.log("Muell is listening on "
                + addr.address + " (" + addr.family + ")"
                + " on port " + addr.port + "..."
                );
    }
);



exports.app = app;
exports.server = server;
